use <threads.scad>

$fn=100;

MM_PER_INCH = 25.4;

SCREW_TAP_HOLE_DIAM_4_40  = 0.0890*MM_PER_INCH;
SCREW_THRU_HOLE_DIAM_4_40 = 0.1285*MM_PER_INCH;

// Gasket Minimum Thickness
GMT = 2;
HT_RESIN_SHRINK_ADJUSTMENT_FACTOR = 1.016;

GASKET_DIAM_COMPRESSION = 0.60;
GASKET_OUTER_DIAM  = 14*GMT + GASKET_DIAM_COMPRESSION;


GASKET_CAVITY_CONE1_HEIGHT = 1.5*GMT;
GASKET_CAVITY_CONE1_DIAM1  = GASKET_OUTER_DIAM - 3*GMT;
GASKET_CAVITY_CONE1_DIAM2  = GASKET_OUTER_DIAM - 5*GMT;

GASKET_LIP_HEIGHT          = 0.5*GMT;
GASKET_LIP_DIAM            = GASKET_CAVITY_CONE1_DIAM2;
GASKET_LIP_OFFSET_Z        = GASKET_CAVITY_CONE1_HEIGHT;

GASKET_CAVITY_SEAT_HEIGHT   = 1*GMT;
GASKET_CAVITY_SEAT_DIAM1    = GASKET_LIP_DIAM+1*GMT;
GASKET_CAVITY_SEAT_DIAM2    = GASKET_CAVITY_CONE1_DIAM1;
GASKET_CAVITY_SEAT_OFFSET_Z = GASKET_LIP_OFFSET_Z + GASKET_LIP_HEIGHT;


GASKET_CAVITY_CONE2_HEIGHT = GASKET_CAVITY_CONE1_HEIGHT;
GASKET_CAVITY_CONE2_DIAM1  = GASKET_CAVITY_SEAT_DIAM2;
GASKET_CAVITY_CONE2_DIAM2  = 5*GMT;
GASKET_CAVITY_CONE2_OFFSET_Z = GASKET_CAVITY_SEAT_OFFSET_Z + GASKET_CAVITY_SEAT_HEIGHT;


GASKET_BASE1_DIAM_GAP = 0;//0.5*GMT;
GASKET_BASE1_HEIGHT = 1*GMT;
GASKET_BASE1_DIAM   = GASKET_OUTER_DIAM - GASKET_BASE1_DIAM_GAP;

GASKET_BASE2_HEIGHT = 1*GMT;
GASKET_BASE2_DIAM1  = GASKET_BASE1_DIAM - 1*GMT;
GASKET_BASE2_DIAM2  = GASKET_OUTER_DIAM;


GASKET_BASE3_HEIGHT = 1.5*GMT;
GASKET_BASE3_DIAM   = GASKET_BASE2_DIAM2;


GASKET_BASE_HEIGHT = GASKET_BASE1_HEIGHT + GASKET_BASE2_HEIGHT + GASKET_BASE3_HEIGHT;

GASKET_TIP_CONE_HEIGHT = 3*GMT;
GASKET_TIP_CONE_DIAM1  = GASKET_OUTER_DIAM;
GASKET_TIP_CONE_DIAM2  = 2*GMT;

GASKET_TOTAL_HEIGHT = GASKET_BASE_HEIGHT + GASKET_TIP_CONE_HEIGHT;
GASKET_TEXT_SIZE = 3;

//------------------------------------------------------------------
SYRINGE_CAVITY_DIAM = HT_RESIN_SHRINK_ADJUSTMENT_FACTOR*(GASKET_OUTER_DIAM - GASKET_DIAM_COMPRESSION);
SYRINGE_WALL_THICKNESS = 2;
SYRINGE_BODY_DIAM = SYRINGE_CAVITY_DIAM + 2*SYRINGE_WALL_THICKNESS;
SYRINGE_BODY_HEIGHT = 80;

SYRINGE_CAVITY_LEAD_IN_DIAM   = 1.05*SYRINGE_CAVITY_DIAM;
SYRINGE_CAVITY_LEAD_IN_HEIGHT = 4;

SYRINGE_FINGER_TAB_MINK_R = 1;
SYRINGE_FINGER_TAB_THICKNESS = 3;
SYRINGE_FINGER_TAB_DIAM = SYRINGE_BODY_DIAM/2;
SYRINGE_FINGER_TAB_CENTER_DIAM = SYRINGE_BODY_DIAM + SYRINGE_WALL_THICKNESS;

SYRINGE_TAPER_END_HEIGHT = 4*GMT;
SYRINGE_TAPER_END_OUTER_DIAM = 10;
SYRINGE_TAPER_END_WALL_THICKNESS = 2;
SYRINGE_TAPER_END_INNER_DIAM = SYRINGE_TAPER_END_OUTER_DIAM - 2*SYRINGE_TAPER_END_WALL_THICKNESS;

SYRINGE_TIP_HEIGHT = 10;
SYRINGE_TIP_OUTER_DIAM = 7;
SYRINGE_TIP_WALL_THICKNESS = SYRINGE_TAPER_END_WALL_THICKNESS/2;
SYRINGE_TIP_INNER_DIAM = SYRINGE_TIP_OUTER_DIAM - 2*SYRINGE_TIP_WALL_THICKNESS;

SYRINGE_LOCK_ARM_DIAM    = 10;
SYRINGE_LOCK_ARM_HEIGHT  = 5;
SYRINGE_LOCK_ARM_OFFSET  = SYRINGE_BODY_DIAM/2 + SYRINGE_LOCK_ARM_DIAM/4;


//------------------------------------------------------------------
SYRINGE_CAP_THICKNESS    = 1;
SYRINGE_CAP_RESTRICTION_DIAM  = 0.5;
SYRINGE_CAP_MINK_R       = 0.25;
SYRINGE_CAP_INNER_DIAM1   = SYRINGE_TAPER_END_OUTER_DIAM - SYRINGE_CAP_RESTRICTION_DIAM;
SYRINGE_CAP_INNER_DIAM2   = SYRINGE_TIP_OUTER_DIAM;

SYRINGE_CAP_OUTER_DIAM1 = SYRINGE_CAP_INNER_DIAM1 + 2*SYRINGE_CAP_THICKNESS - 2*SYRINGE_CAP_MINK_R;
SYRINGE_CAP_OUTER_DIAM2 = SYRINGE_CAP_INNER_DIAM2 + 2*SYRINGE_CAP_THICKNESS - 2*SYRINGE_CAP_MINK_R;

SYRINGE_CAP_GRIP_ANGLE   = 10;
SYRINGE_CAP_GRIP_R       = 0.5;
SYRINGE_CAP_GRIP_HEIGHT  = 3*SYRINGE_CAP_THICKNESS - 2*SYRINGE_CAP_MINK_R;
SYRINGE_CAP_GRIP_OUTER_DIAM  = SYRINGE_CAP_OUTER_DIAM1 + 2*SYRINGE_CAP_THICKNESS - 2*SYRINGE_CAP_MINK_R;
SYRINGE_CAP_HEIGHT       = SYRINGE_TIP_HEIGHT + 2*SYRINGE_CAP_THICKNESS - 2*SYRINGE_CAP_MINK_R;

SYRINGE_CAP_LIP_HEIGHT              = 0.25;
SYRINGE_CAP_LIP_R_RESTRICTION       = 0.25;
SYRINGE_CAP_LIP_Z_OFFSET            = SYRINGE_CAP_GRIP_HEIGHT/2-SYRINGE_CAP_LIP_HEIGHT/2;
SYRINGE_CAP_TOP_GAP                 = 0.5;
SYRINGE_CAP_CONICAL_CAVITY_Z_OFFSET = SYRINGE_CAP_GRIP_HEIGHT/2+SYRINGE_CAP_LIP_HEIGHT/2;
SYRINGE_CAP_CONICAL_CAVITY_HEIGHT   = SYRINGE_TIP_HEIGHT - SYRINGE_CAP_GRIP_HEIGHT/2+ SYRINGE_CAP_TOP_GAP; //there should be a gap of about SYRINGE_CAP_GRIP_HEIGHT/2 at the top

SYRINGE_CAP_GASKET_THROAT_DIAM = SYRINGE_TIP_INNER_DIAM;
//--------------------------------------------------------------------------
PLUNGER_VENT_NUM     = 4;
PLUNGER_VENT_ANGLE   = 55;
PLUNGER_VENT_DIAM    = 2;
PLUNGER_GASKET_DIAM_OVERAGE  = 0.4;
PLUNGER_DIAM_GAP = 0.1;
PLUNGER_WALL_THICKNESS = 3;


PLUNGER_SHAFT_HEIGHT = SYRINGE_BODY_HEIGHT + 10;
PLUNGER_SHAFT_DIAM   = 4*PLUNGER_WALL_THICKNESS;

PLUNGER_GASKET_RING_DIAM1     = GASKET_CAVITY_SEAT_DIAM1;
PLUNGER_GASKET_RING_DIAM2     = GASKET_CAVITY_SEAT_DIAM2;
PLUNGER_GASKET_RING_HEIGHT    = GASKET_CAVITY_SEAT_HEIGHT;
PLUNGER_GASKET_RING_OFFSET_Z  = PLUNGER_SHAFT_HEIGHT;

PLUNGER_CUSHION_RING_GAP      = 0.5;
PLUNGER_CUSHION_RING_DIAM     = GASKET_LIP_DIAM;
PLUNGER_CUSHION_RING_HEIGHT   = GASKET_CAVITY_CONE1_HEIGHT + GASKET_LIP_HEIGHT;
PLUNGER_CUSHION_RING_OFFSET_Z = PLUNGER_SHAFT_HEIGHT - PLUNGER_CUSHION_RING_HEIGHT;

PLUNGER_BACKSTOP_RING_DIAM     = PLUNGER_SHAFT_HEIGHT - 2*PLUNGER_WALL_THICKNESS;
PLUNGER_BACKSTOP_RING_HEIGHT   = PLUNGER_WALL_THICKNESS;
PLUNGER_BACKSTOP_RING_OFFSET_Z = PLUNGER_CUSHION_RING_OFFSET_Z - PLUNGER_BACKSTOP_RING_HEIGHT;

PLUNGER_CENTER_RING_DIAM     = GASKET_OUTER_DIAM - GASKET_DIAM_COMPRESSION - PLUNGER_DIAM_GAP;
PLUNGER_CENTER_RING_OFFSET_Z = PLUNGER_BACKSTOP_RING_OFFSET_Z - 15;

PLUNGER_HANDLE_MINK_R = 0.5;
PLUNGER_HANDLE_DIAM = PLUNGER_CENTER_RING_DIAM;
PLUNGER_HANDLE_HEIGHT = 2*PLUNGER_WALL_THICKNESS;

VENT_SCREW_VALVE_DIAM   = 5;
VENT_SCREW_VALVE_LENGTH = 10;
VENT_SCREW_VALVE_HEAD_HEIGHT = 6;
VENT_SCREW_VALVE_HEAD_DIAM   = 10;

VENT_SCREW_VALVE_GASKET_HEIGHT = 1.5;
VENT_SCREW_VALVE_GASKET_COMPESSION    = 0.5;

VENT_SCREW_VALVE_GASKET_INNER_DIAM   = 0.95*VENT_SCREW_VALVE_DIAM; //stretch a little to hold on
VENT_SCREW_VALVE_GASKET_OUTER_DIAM   = VENT_SCREW_VALVE_DIAM + 2*VENT_SCREW_VALVE_GASKET_HEIGHT;

VENT_SCREW_VALVE_GASKET_FLANGE_HEIGHT = 2;
VENT_SCREW_VALVE_GASKET_FLANGE_DIAM   = VENT_SCREW_VALVE_GASKET_INNER_DIAM + VENT_SCREW_VALVE_GASKET_COMPESSION;
VENT_SCREW_VALVE_GASKET_SEAT_DEPTH    = VENT_SCREW_VALVE_GASKET_HEIGHT - VENT_SCREW_VALVE_GASKET_COMPESSION;
VENT_SCREW_VALVE_GASKET_SEAT_DIAM     = 1.1*VENT_SCREW_VALVE_GASKET_OUTER_DIAM;
VENT_SCREW_VALVE_GRIP_ANGLE  = SYRINGE_CAP_GRIP_ANGLE;
VENT_SCREW_VALVE_GRIP_R = SYRINGE_CAP_GRIP_R;
VENT_SCREW_VALVE_VENT_DIAM = 1.5;

LOCK_STAND_BODY_DIAM   = SYRINGE_BODY_DIAM;
LOCK_STAND_BODY_HEIGHT = SYRINGE_TIP_HEIGHT + 5;
LOCK_STAND_ARM_DIAM    = 10;
LOCK_STAND_ARM_HEIGHT  = LOCK_STAND_BODY_HEIGHT + SYRINGE_TAPER_END_HEIGHT;
LOCK_STAND_ARM_OFFSET  = SYRINGE_BODY_DIAM/2 + LOCK_STAND_ARM_DIAM/4;

SYRINGE_PORT_MOUTH_R1 = 10.0/2; //custom topload syringe with cap gasket under compression
SYRINGE_PORT_MOUTH_R2 = 9.0/2;
SYRINGE_PORT_MOUTH_H  = 8.0;


module tube(inner_r,outer_r, h){
    difference(){
        cylinder(r = outer_r, h = h);
        translate([0,0,-0.1])
        cylinder(r = inner_r, h = h+0.2);
    }
}

module syringe_finger_tabs(){
    translate([0,0,SYRINGE_FINGER_TAB_MINK_R])
    minkowski(){
    hull(){
        thickness = SYRINGE_FINGER_TAB_THICKNESS;
        //right side
        translate([SYRINGE_FINGER_TAB_DIAM,SYRINGE_FINGER_TAB_DIAM/4,0])
        cylinder(h=thickness,r=SYRINGE_FINGER_TAB_DIAM/2);
        translate([SYRINGE_FINGER_TAB_DIAM,-SYRINGE_FINGER_TAB_DIAM/4,0])
        cylinder(h=thickness,r=SYRINGE_FINGER_TAB_DIAM/2);
        //center
        cylinder(h=thickness,r=SYRINGE_FINGER_TAB_CENTER_DIAM/2);
        //left side
        translate([-SYRINGE_FINGER_TAB_DIAM,SYRINGE_FINGER_TAB_DIAM/4,0])
        cylinder(h=thickness,r=SYRINGE_FINGER_TAB_DIAM/2);
        translate([-SYRINGE_FINGER_TAB_DIAM,-SYRINGE_FINGER_TAB_DIAM/4,0])
        cylinder(h=thickness,r=SYRINGE_FINGER_TAB_DIAM/2);
        
    }
    sphere(SYRINGE_FINGER_TAB_MINK_R);
    }
}

module syringe_body(){
    difference(){
        union(){
            cylinder(h=SYRINGE_BODY_HEIGHT,r=SYRINGE_BODY_DIAM/2);
            syringe_finger_tabs();
            //lock arms
            translate([0,0,SYRINGE_BODY_HEIGHT-SYRINGE_LOCK_ARM_HEIGHT])
            hull(){
                translate([0,SYRINGE_LOCK_ARM_OFFSET,0])
                cylinder(h=SYRINGE_LOCK_ARM_HEIGHT,r=SYRINGE_LOCK_ARM_DIAM/2);
                translate([0,-SYRINGE_LOCK_ARM_OFFSET,0])
                cylinder(h=SYRINGE_LOCK_ARM_HEIGHT,r=SYRINGE_LOCK_ARM_DIAM/2);
            }
        }
        //cavity lead in
        translate([0,0,-0.01])
        cylinder(h=SYRINGE_CAVITY_LEAD_IN_HEIGHT+0.02,r1=SYRINGE_CAVITY_LEAD_IN_DIAM/2,r2=SYRINGE_CAVITY_DIAM/2);
        // cavity
        translate([0,0,-0.01])
        cylinder(h=SYRINGE_BODY_HEIGHT+0.02,r=SYRINGE_CAVITY_DIAM/2);
        //screw thru holes
        translate([0,SYRINGE_LOCK_ARM_OFFSET,SYRINGE_BODY_HEIGHT-SYRINGE_LOCK_ARM_HEIGHT])
        #cylinder(h=3*SYRINGE_LOCK_ARM_HEIGHT,r=SCREW_THRU_HOLE_DIAM_4_40/2,center=true);
        translate([0,-SYRINGE_LOCK_ARM_OFFSET,SYRINGE_BODY_HEIGHT-SYRINGE_LOCK_ARM_HEIGHT])
        #cylinder(h=3*SYRINGE_LOCK_ARM_HEIGHT,r=SCREW_THRU_HOLE_DIAM_4_40/2,center=true);
    }
}



module syringe_taper(){
    difference(){
        cylinder(h=SYRINGE_TAPER_END_HEIGHT,r1=SYRINGE_BODY_DIAM/2, r2=SYRINGE_TAPER_END_OUTER_DIAM/2);
        // cavity
        translate([0,0,-0.01])
        cylinder(h=SYRINGE_TAPER_END_HEIGHT+0.02,r1=SYRINGE_CAVITY_DIAM/2,r2=SYRINGE_TAPER_END_INNER_DIAM/2);
    }
}


module syringe_tip(){
    difference(){
        cylinder(h=SYRINGE_TIP_HEIGHT,r1=SYRINGE_TAPER_END_OUTER_DIAM/2, r2=SYRINGE_TIP_OUTER_DIAM/2);
        // cavity
        translate([0,0,-0.01])
        cylinder(h=SYRINGE_TIP_HEIGHT+0.02,r1=SYRINGE_TAPER_END_INNER_DIAM/2,r2=SYRINGE_TIP_INNER_DIAM/2);
        //cap lip mating trench
        translate([0,0,SYRINGE_CAP_LIP_Z_OFFSET])
        tube(SYRINGE_TAPER_END_OUTER_DIAM/2-2*SYRINGE_CAP_LIP_R_RESTRICTION,
          SYRINGE_TAPER_END_OUTER_DIAM/2,h=2*SYRINGE_CAP_LIP_HEIGHT);
    }
}

module syringe(){
    syringe_body();
    translate([0,0,SYRINGE_BODY_HEIGHT-0.01])
    syringe_taper();
    translate([0,0,SYRINGE_BODY_HEIGHT+SYRINGE_TAPER_END_HEIGHT-0.01])
    syringe_tip();
    
}

//-------------------------------
module syringe_cap(){
    difference(){
        translate([0,0,SYRINGE_CAP_MINK_R])
        union(){           
            minkowski(){
                union(){
                    cylinder(h=SYRINGE_CAP_HEIGHT,r1=SYRINGE_CAP_OUTER_DIAM1/2,r2=SYRINGE_CAP_OUTER_DIAM2/2);
                    //grip
                    cylinder(h=SYRINGE_CAP_GRIP_HEIGHT,r=SYRINGE_CAP_GRIP_OUTER_DIAM/2);
                }
                sphere(SYRINGE_CAP_MINK_R);
            }
            for(a = [0:SYRINGE_CAP_GRIP_ANGLE:360]){
                rotate([0,0,a])
                translate([SYRINGE_CAP_GRIP_OUTER_DIAM/2,0,0])
                cylinder(h=SYRINGE_CAP_GRIP_HEIGHT,r=SYRINGE_CAP_GRIP_R);
            }
            //lip to snap in
            
            
        }
        translate([0,0,-0.01])
        cylinder(h=SYRINGE_CAP_GRIP_HEIGHT/2-SYRINGE_CAP_LIP_HEIGHT/2 + 0.02,r=SYRINGE_CAP_INNER_DIAM1/2);
        //lip restriction 
        translate([0,0,SYRINGE_CAP_LIP_Z_OFFSET-0.01])
        cylinder(h=SYRINGE_CAP_LIP_HEIGHT+0.02,r=SYRINGE_CAP_INNER_DIAM1/2-SYRINGE_CAP_LIP_R_RESTRICTION);
        //conical cup
        translate([0,0,SYRINGE_CAP_CONICAL_CAVITY_Z_OFFSET-0.01])
        cylinder(h=SYRINGE_CAP_CONICAL_CAVITY_HEIGHT,r1=SYRINGE_CAP_INNER_DIAM1/2,r2=SYRINGE_CAP_INNER_DIAM2/2);
   }
}

module syringe_cap_gasket(){
    difference(){
        syringe_cap();
        //throat
        translate([0,0,-SYRINGE_TIP_HEIGHT+0.01])
        cylinder(h=3*SYRINGE_TIP_HEIGHT,r=SYRINGE_CAP_GASKET_THROAT_DIAM/2);
    }
}
//-------------------------------
module plunger(){
    PLUNGER_CLEARANCE_HOLE_LENGTH = 3;
    PLUNGER_SCREW_HOLE_LENGTH     = 1.2*VENT_SCREW_VALVE_LENGTH;
    ClearanceHole(VENT_SCREW_VALVE_GASKET_SEAT_DIAM,VENT_SCREW_VALVE_GASKET_SEAT_DEPTH,position=[0,0,-.01],tolerance=0) //gasket seat
    ClearanceHole(VENT_SCREW_VALVE_DIAM,PLUNGER_CLEARANCE_HOLE_LENGTH,position=[0,0,-.01])
    ScrewHole(VENT_SCREW_VALVE_DIAM,PLUNGER_SCREW_HOLE_LENGTH,position=[0,0,PLUNGER_CLEARANCE_HOLE_LENGTH-.01])
    difference(){
        union(){
            //shaft
            cylinder(h=PLUNGER_SHAFT_HEIGHT,r=PLUNGER_SHAFT_DIAM/2);
            //centering ring
            translate([0,0,PLUNGER_CENTER_RING_OFFSET_Z])
            cylinder(h=PLUNGER_WALL_THICKNESS,r=PLUNGER_CENTER_RING_DIAM/2);
            //gasket backstop ring
            translate([0,0,PLUNGER_BACKSTOP_RING_OFFSET_Z])
            cylinder(h=PLUNGER_WALL_THICKNESS,r=PLUNGER_CENTER_RING_DIAM/2);
            //gasket cushion ring
            translate([0,0,PLUNGER_CUSHION_RING_OFFSET_Z])
            cylinder(h=PLUNGER_CUSHION_RING_HEIGHT,r=PLUNGER_CUSHION_RING_DIAM/2);
            //gasket seat ring
            translate([0,0,PLUNGER_GASKET_RING_OFFSET_Z-0.01])
            cylinder(h=PLUNGER_GASKET_RING_HEIGHT,r1=PLUNGER_GASKET_RING_DIAM1/2,r2=PLUNGER_GASKET_RING_DIAM2/2);         
            //handle
            translate([0,0,PLUNGER_HANDLE_MINK_R])
            minkowski(){
                cylinder(h=PLUNGER_HANDLE_HEIGHT-PLUNGER_HANDLE_MINK_R,r=(PLUNGER_HANDLE_DIAM-2*PLUNGER_HANDLE_MINK_R)/2);
                sphere(PLUNGER_HANDLE_MINK_R);
            }
           
        }
        //vent
        translate([0,0,-0.5*PLUNGER_SHAFT_HEIGHT])
        cylinder(h=2*PLUNGER_SHAFT_HEIGHT,r=PLUNGER_VENT_DIAM/2);
    }
}

//---------------------------------

module gasket(){
    difference(){
        union(){
            //gasket base
            cylinder(h=GASKET_BASE1_HEIGHT,r=GASKET_BASE1_DIAM/2);
            translate([0,0,GASKET_BASE1_HEIGHT-0.01])
            cylinder(h=GASKET_BASE2_HEIGHT,r1=GASKET_BASE2_DIAM1/2,r2=GASKET_BASE2_DIAM2/2);
            translate([0,0,GASKET_BASE1_HEIGHT + GASKET_BASE2_HEIGHT-0.01])
            cylinder(h=GASKET_BASE3_HEIGHT,r=GASKET_BASE3_DIAM/2);
            //gasket tip cone
            translate([0,0,GASKET_BASE_HEIGHT-0.01])
            cylinder(h=GASKET_TIP_CONE_HEIGHT,r1=GASKET_TIP_CONE_DIAM1/2,r2=GASKET_TIP_CONE_DIAM2/2);
     
        }
        //first cavity cone
        translate([0,0,-0.01])
        cylinder(h=GASKET_CAVITY_CONE1_HEIGHT + 0.02,r1=GASKET_CAVITY_CONE1_DIAM1/2,r2=GASKET_CAVITY_CONE1_DIAM2/2);
        //lip cavity
        translate([0,0,GASKET_LIP_OFFSET_Z-0.01])
        cylinder(h=GASKET_LIP_HEIGHT + 0.02,r=GASKET_LIP_DIAM/2);
        //cavity seat cone
        translate([0,0,GASKET_CAVITY_SEAT_OFFSET_Z-0.01])
        cylinder(h=GMT + 0.02,r1=GASKET_CAVITY_SEAT_DIAM1/2,r2=GASKET_CAVITY_SEAT_DIAM2/2);
        //second cavity cone
        translate([0,0,GASKET_CAVITY_CONE2_OFFSET_Z-0.01])
        cylinder(h=GASKET_CAVITY_CONE2_HEIGHT + 0.02,r1=GASKET_CAVITY_CONE2_DIAM1/2,r2=GASKET_CAVITY_CONE2_DIAM2/2);
        //vent
        
        for(i=[0:1:PLUNGER_VENT_NUM]){
            rot_z = i*360/PLUNGER_VENT_NUM;
            rotate([PLUNGER_VENT_ANGLE,0,rot_z])
            translate([0,0,-0.01])
            cylinder(h=3*GASKET_BASE_HEIGHT,r=PLUNGER_VENT_DIAM/2);
        }
        
    }
    //label with GASKET_DIAM_COMPRESSION
    translate([0,0,GASKET_CAVITY_CONE2_OFFSET_Z+GASKET_CAVITY_CONE2_HEIGHT+0.25])
    rotate([0,180,0])
    linear_extrude(height = 0.5) {
        text(str(GASKET_DIAM_COMPRESSION),size=GASKET_TEXT_SIZE,halign="center",valign="center");
    }
}



module vent_screw_valve_head(with_gasket_seat=true){
    difference(){
        union(){
            //body
            cylinder(h=VENT_SCREW_VALVE_HEAD_HEIGHT,r=VENT_SCREW_VALVE_HEAD_DIAM/2);
            //finger grips
            for(a = [0:SYRINGE_CAP_GRIP_ANGLE:360]){
                rotate([0,0,a])
                translate([VENT_SCREW_VALVE_HEAD_DIAM/2,0,0])
                cylinder(h=VENT_SCREW_VALVE_HEAD_HEIGHT,r=SYRINGE_CAP_GRIP_R);
            }
        }
        //negatives
        if(with_gasket_seat){
            translate([0,0,VENT_SCREW_VALVE_HEAD_HEIGHT-VENT_SCREW_VALVE_GASKET_SEAT_DEPTH])
            #cylinder(h=1.1*VENT_SCREW_VALVE_GASKET_SEAT_DEPTH,r=VENT_SCREW_VALVE_GASKET_SEAT_DIAM/2);
        }
    }
}


module vent_screw_valve(with_gasket_seat=true){
   difference(){
       union(){
            //head
            vent_screw_valve_head(with_gasket_seat);
            //gasket flange
            translate([0,0,VENT_SCREW_VALVE_HEAD_HEIGHT-VENT_SCREW_VALVE_GASKET_SEAT_DEPTH-0.01])
            cylinder(h=VENT_SCREW_VALVE_GASKET_FLANGE_HEIGHT,r=VENT_SCREW_VALVE_GASKET_FLANGE_DIAM/2);
            //screw thread
            translate([0,0,VENT_SCREW_VALVE_HEAD_HEIGHT-VENT_SCREW_VALVE_GASKET_SEAT_DEPTH+VENT_SCREW_VALVE_GASKET_FLANGE_HEIGHT-0.01])
            ScrewThread(VENT_SCREW_VALVE_DIAM, VENT_SCREW_VALVE_LENGTH, 
                        tolerance=0.4,
                        tip_height=ThreadPitch(VENT_SCREW_VALVE_DIAM), 
                        tip_min_fract=0.75);
       }
       //vent cavities
       translate([0,0,VENT_SCREW_VALVE_HEAD_HEIGHT-VENT_SCREW_VALVE_GASKET_SEAT_DEPTH+VENT_SCREW_VALVE_GASKET_FLANGE_HEIGHT+VENT_SCREW_VALVE_VENT_DIAM])
       rotate([90,0,0])
       cylinder(h=2*VENT_SCREW_VALVE_DIAM,r=VENT_SCREW_VALVE_VENT_DIAM/2*1/sqrt(2),center=true); //there are two vents so we can dowmsize diam
       translate([0,0,VENT_SCREW_VALVE_HEAD_HEIGHT-VENT_SCREW_VALVE_GASKET_SEAT_DEPTH+VENT_SCREW_VALVE_GASKET_FLANGE_HEIGHT+VENT_SCREW_VALVE_VENT_DIAM])
       cylinder(h=2*VENT_SCREW_VALVE_LENGTH,r=VENT_SCREW_VALVE_VENT_DIAM/2);
   }
}

module vent_screw_valve_gasket(){
   tube(VENT_SCREW_VALVE_GASKET_INNER_DIAM/2, VENT_SCREW_VALVE_GASKET_OUTER_DIAM/2,VENT_SCREW_VALVE_GASKET_HEIGHT);
}

module vent_screw_valve_with_gasket(){
    //merge the gasket and the screw
    vent_screw_valve(with_gasket_seat=false);
    translate([0,0,VENT_SCREW_VALVE_HEAD_HEIGHT-VENT_SCREW_VALVE_GASKET_SEAT_DEPTH])
    vent_screw_valve_gasket();
}

//---------------------------------




module lock_stand(){
    difference(){
        union(){
            cylinder(h=LOCK_STAND_BODY_HEIGHT,r=LOCK_STAND_BODY_DIAM/2);
            hull(){
                translate([0,LOCK_STAND_ARM_OFFSET,0])
                cylinder(h=LOCK_STAND_ARM_HEIGHT,r=LOCK_STAND_ARM_DIAM/2);
                translate([0,-LOCK_STAND_ARM_OFFSET,0])
                cylinder(h=LOCK_STAND_ARM_HEIGHT,r=LOCK_STAND_ARM_DIAM/2);
            }
            
        }
        //cut away for syringe taper
        translate([0,0,LOCK_STAND_BODY_HEIGHT])
        cylinder(h=LOCK_STAND_BODY_HEIGHT,r=LOCK_STAND_BODY_DIAM/2);
        //mouth for syringe with gasket_cap
        translate([0,0,LOCK_STAND_BODY_HEIGHT+0.01])
        rotate([180,0,0])
        cylinder(h=SYRINGE_PORT_MOUTH_H,r1=SYRINGE_PORT_MOUTH_R1,r2=SYRINGE_PORT_MOUTH_R2);
        //screw tap holes
        translate([0,LOCK_STAND_ARM_OFFSET,0])
        #cylinder(h=3*LOCK_STAND_ARM_HEIGHT,r=SCREW_TAP_HOLE_DIAM_4_40/2,center=true);
        translate([0,-LOCK_STAND_ARM_OFFSET,0])
        #cylinder(h=3*LOCK_STAND_ARM_HEIGHT,r=SCREW_TAP_HOLE_DIAM_4_40/2,center=true);
    }
}


// --- DEBUGGING
module debug_vent_screw_valve_gasket(){
  color("green") vent_screw_valve_gasket();
  translate([0,0,1*VENT_SCREW_VALVE_GASKET_HEIGHT]) plunger();
}


module debug_gasket(){
    //cut_away view
    difference(){
        gasket();
        translate([-15,0,-1])
        cube(30);
    }
    translate([0,0,-PLUNGER_SHAFT_HEIGHT+PLUNGER_CUSHION_RING_HEIGHT])
    plunger();
}

module debug_syringe_cap_cutaway(){
    difference(){
        syringe_cap();
        translate([-15,0,-1])
        cube(30);
    }
}


module debug_syringe_cap(){
    //cut_away view
    debug_syringe_cap_cutaway();
    //shifted cutaway view
    translate([-15,0,0])
    debug_syringe_cap_cutaway();
    translate([-0,0,-SYRINGE_BODY_HEIGHT-SYRINGE_TAPER_END_HEIGHT])
    syringe();
  
}

module debug_lock_stand(){
    lock_stand();
    translate([0,0,SYRINGE_BODY_HEIGHT+LOCK_STAND_BODY_HEIGHT+10])
    rotate([180,0,0])
    syringe();
}
//---------------------------------
module assembly(){
    translate([0,0,1.2*PLUNGER_SHAFT_HEIGHT]) syringe_cap();
    syringe();
    translate([0,0,-3*GASKET_BASE_HEIGHT]) gasket();
    translate([0,0,-1.4*PLUNGER_SHAFT_HEIGHT]) plunger();
    translate([0,0,-1.6*PLUNGER_SHAFT_HEIGHT]) vent_screw_valve();
}



//syringe();
//syringe_cap();
//syringe_cap_gasket();
vent_screw_valve();
//vent_screw_valve_gasket();
//vent_screw_valve_with_gasket();
//gasket();
//lock_stand();
//debug_lock_stand();

//debug_gasket();
//plunger();
//debug_syringe_cap();


//assembly();