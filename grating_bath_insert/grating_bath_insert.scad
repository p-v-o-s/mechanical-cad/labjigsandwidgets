$fn = 100;

module grating_insert(diam,thickness,height,hole_diam,hole_spacing){
    //ring around edge
    translate([0,0,height/2])
    difference(){
        cylinder(h=height, r=diam/2, center = true);
        cylinder(h=height+0.1, r=(diam-thickness)/2, center = true);
    }
    translate([0,0,height - thickness/2])
    difference(){
        //body
        cylinder(h=thickness, r=diam/2, center = true);
        //subtracted
        n_holes = diam/hole_spacing;
        v0 = [diam/2,diam/2,0];
        for (i = [0:n_holes]){
            for (j = [0:n_holes]){
                v = [i*hole_spacing,j*hole_spacing,0];
                translate(v - v0)
                cylinder(h=thickness + 0.1, r=hole_diam/2, center=true, $fn=20);
            }
        }
    }
}

//fits 40ml polypropylene specimen container, comes up to 20ml mark
grating_insert(41.5,2,13,1.75,2);