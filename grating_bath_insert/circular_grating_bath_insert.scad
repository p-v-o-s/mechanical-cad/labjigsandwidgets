$fn = 100;


module ring(diam,thickness,height){
    difference(){
        cylinder(h=height, r=diam/2, center = true);
        cylinder(h=height+0.1, r=(diam-thickness)/2, center = true);
    }
}

module grating_insert(diam,ring_thickness,ring_spacing,height){
    //ring around edge
    translate([0,0,height/2])
    ring(diam=diam, thickness=2*ring_thickness,height=height + 0.1);

    translate([0,0,height/2 - ring_thickness/2])
    union(){
        //body
        //subtracted
        n_rings = diam/(2*ring_spacing);
        v0 = [diam/2,diam/2,0];
        for (i = [0:n_rings]){
            ring(diam=2*i*ring_spacing, thickness=ring_spacing,height=ring_thickness + 0.1);
        }
        //support spokes
        rotate([90,0,0])
        cylinder(h=diam,r=ring_thickness/4,center=true);
        rotate([90,0,90])
        cylinder(h=diam,r=ring_thickness/4,center=true);
        
    }
}

//fits 40ml polypropylene specimen container, comes up to 20ml mark
grating_insert(41.5,2,2,26);