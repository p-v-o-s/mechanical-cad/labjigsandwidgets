function reverse(v) =
    let(max=len(v) -1)
    [ for (i = [0:max])  v[max - i]  ];
     
function parabola_points(x_max=1,a=1,x0=0,y0=0,x_offset=0,res=10) = 
    let(step=x_max/res)
    [for(x=[x_offset:step:x_max])[x+x0,a*(x+x0)^2 + y0]];   
    
    
module rot_extrude_polygon(points,rot_fn=20){
    rotate_extrude($fn=rot_fn, convexity=10) polygon(points=points);
}

module parabolic_dish(r=10,t=0.1,focal_length=20,curve_res=10,rot_fn=20){
    a = 1/(4*focal_length);                                           //scale factor
    pp1 = parabola_points(x_max=r,a=a,res=curve_res);                 //points from bottom to top
    pp2 = reverse(parabola_points(x_max=r,a=a,y0=-t,res=curve_res));  //shift down by thickness, continue points path from top to bottom
    poly_points = concat(pp1,pp2);     //polygon will follow points back down
    rot_extrude_polygon(poly_points,rot_fn=rot_fn);
}

module plano_concave_paraboloid(r=10,focal_length=20,extra_thickness=0,x_offset=0,curve_res=10,rot_fn=20){
    a = 1/(4*focal_length);                           //scale factor
    pp1 = parabola_points(x_max=r,a=a,y0=extra_thickness,x_offset=x_offset,res=curve_res); //points from bottom to top
    poly_points = concat(pp1,[[r,0],[x_offset,0]]);                  //polygon drops down to X axis
    rot_extrude_polygon(poly_points,rot_fn=rot_fn);
}

module plano_convex_paraboloid(r=10,focal_length=20,extra_thickness=0,curve_res=10,rot_fn=20){
    a = -1/(4*focal_length);                           //scale factor
    curve_h = -a*r^2;
    total_h = curve_h+extra_thickness;
    pp1 = parabola_points(x_max=r,a=a,y0=total_h,res=curve_res); //points from bottom to top
    poly_points = concat(pp1,[[r,0],[0,0]]);                  //polygon drops down to X axis
    translate([0,0,-total_h])
    rot_extrude_polygon(poly_points,rot_fn=rot_fn);
}

