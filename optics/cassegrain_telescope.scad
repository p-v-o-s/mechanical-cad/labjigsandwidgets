use <parabolic_optics.scad>;

$fn=100;

//parabolic_dish(r=10,t=1,focal_length=10, curve_res=100, rot_fn=200);
PRIMARY_DIAM = 150;
PRIMARY_VIEWPORT_DIAM = 20;
PRIMARY_FOCAL_LENGTH = 300;
PRIMARY_EXTRA_THICKNESS = 2;


SECONDARY_DIAM = 50;
SECONDARY_FOCAL_LENGTH = 50;
SECONDARY_EXTRA_THICKNESS = 2;

module primary_mirror(){
    plano_concave_paraboloid(r=PRIMARY_DIAM/2,
                             focal_length=PRIMARY_FOCAL_LENGTH,
                             x_offset=PRIMARY_VIEWPORT_DIAM/2,  //this creates a hole in the paraboloid
                             extra_thickness=PRIMARY_EXTRA_THICKNESS,
                             curve_res=100, rot_fn=200);
}

module secondary_mirror(){
    
    plano_convex_paraboloid(r=SECONDARY_DIAM/2,focal_length=SECONDARY_FOCAL_LENGTH, extra_thickness=SECONDARY_EXTRA_THICKNESS, curve_res=100, rot_fn=200);
}

//primary_mirror();

secondary_mirror();