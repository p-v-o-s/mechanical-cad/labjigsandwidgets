$fn = 100;
INCH_TO_MM = 25.4;
BODY_HEIGHT = 0.83*INCH_TO_MM;
BODY_OUTTER_DIAM = 1*INCH_TO_MM;
BODY_INNER_DIAM = 0.301*INCH_TO_MM;
FLANGE_DIAM = 1.25*INCH_TO_MM;
FLANGE_HEIGHT = 0.1*INCH_TO_MM;
SIDE_HOLE_DIAM = 0.18*INCH_TO_MM;
SIDE_HOLE_HEIGHT =  BODY_HEIGHT - FLANGE_HEIGHT - 0.125*INCH_TO_MM;


difference(){
    union(){
        //body
        cylinder(h=BODY_HEIGHT,r=BODY_OUTTER_DIAM/2);
        //flange
        translate([0,0,BODY_HEIGHT-FLANGE_HEIGHT])
        cylinder(h=FLANGE_HEIGHT,r=FLANGE_DIAM/2);
    }
    //holes
    //main cavity
    translate([0,0,-BODY_HEIGHT/2])
    cylinder(h=2*BODY_HEIGHT,r=BODY_INNER_DIAM/2);
    //set screw hole
    translate([0,0,SIDE_HOLE_HEIGHT])
    rotate([90,0,0])
    #cylinder(h=BODY_OUTTER_DIAM,r=SIDE_HOLE_DIAM/2);
    
}