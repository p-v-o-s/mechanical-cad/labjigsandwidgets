$fn = 100;

DISC_DIAM      = 3.5;
DISC_THICKNESS = 0.05;

module disc(){
    cylinder(r=DISC_DIAM/2,h=DISC_THICKNESS);
}

disc();