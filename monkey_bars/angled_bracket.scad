$fn = 100;
IN_TO_MM = 25.4;


PEG_DIAM = 0.95*1*IN_TO_MM; 
PEG_LEN  = 4*IN_TO_MM;
CAP_DIAM = 0.80*1*IN_TO_MM; 
CAP_LEN  = 0.25*IN_TO_MM;
CAP_MINK_R = 0.125*IN_TO_MM; 
BRACKET_ANGLE = -45;
BRACKET_LEN  = 2*IN_TO_MM;
PLATE_WIDTH  = 2*IN_TO_MM; 
PLATE_LEN    = 1*IN_TO_MM; 
PLATE_HEIGHT = 0.25*IN_TO_MM;
SCREW_X_OFFSET = 0.5*IN_TO_MM;
SCREW_THRU_HOLE_DIAM       = 0.25*IN_TO_MM; // 1/4 20 machine screw
SCREW_HEAD_CLEARANCE_DIAM  = 0.40*IN_TO_MM; // 1/4 20 machine screw


module angled_bracket(){
    difference(){
        union(){
            hull(){
                translate([0,0,PLATE_HEIGHT/2])
                cube([PLATE_WIDTH,PLATE_LEN,PLATE_HEIGHT],center=true);
                rotate([BRACKET_ANGLE,0,0])
                    cylinder(r=PEG_DIAM/2,h=BRACKET_LEN);
            }
            //peg
            rotate([BRACKET_ANGLE,0,0])
            translate([0,0,BRACKET_LEN])
                cylinder(r=PEG_DIAM/2,h=PEG_LEN);
            //cap
            rotate([BRACKET_ANGLE,0,0])
            translate([0,0,BRACKET_LEN+PEG_LEN])
            minkowski(){
                cylinder(r1=PEG_DIAM/2-CAP_MINK_R,r2=CAP_DIAM/2-CAP_MINK_R,h=CAP_LEN);
                sphere(r=CAP_MINK_R);
            }
        }
        //flatten base
        translate([0,0,-PLATE_HEIGHT])
        cube([2*PLATE_WIDTH,2*PLATE_LEN,2*PLATE_HEIGHT],center=true);
        //thru holes
        translate([SCREW_X_OFFSET,0,0])
        #cylinder(r=SCREW_THRU_HOLE_DIAM/2,h=20*PLATE_HEIGHT,center=true);
        translate([-SCREW_X_OFFSET,0,0])
        #cylinder(r=SCREW_THRU_HOLE_DIAM/2,h=20*PLATE_HEIGHT,center=true);
        //head clearance holes
        translate([SCREW_X_OFFSET,0,PLATE_HEIGHT])
        #cylinder(r=SCREW_HEAD_CLEARANCE_DIAM/2,h=20*PLATE_HEIGHT);
        translate([-SCREW_X_OFFSET,0,PLATE_HEIGHT])
        #cylinder(r=SCREW_HEAD_CLEARANCE_DIAM/2,h=20*PLATE_HEIGHT);
    }
}

STANDOFF_INNER_DIAM = 1*IN_TO_MM; 
STANDOFF_THICKNESS  = 0.25*IN_TO_MM; 
STANDOFF_OUTER_DIAM = STANDOFF_INNER_DIAM + 2*STANDOFF_THICKNESS;
STANDOFF_LEN = 1*IN_TO_MM;

module standoff(){
    difference(){
        cylinder(r=STANDOFF_OUTER_DIAM/2,h=STANDOFF_LEN);
        //hole
        cylinder(r=STANDOFF_INNER_DIAM/2,h=3*STANDOFF_LEN, center=true);     
    }
    
}

module test_standoff(){
    rotate([BRACKET_ANGLE,0,0])
    translate([0,0,BRACKET_LEN])
        standoff();
}

angled_bracket();
//test_standoff();
//standoff();