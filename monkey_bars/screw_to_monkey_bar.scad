use <threads.scad>


$fn = 100;
MM_PER_INCH = 25.4;

SCREW_THREADS_PER_INCH = 16;       //Coarse
SCREW_DIAM  = 0.375*MM_PER_INCH;   //3/8"
SCREW_PITCH = MM_PER_INCH/SCREW_THREADS_PER_INCH;  
SCREW_HEIGHT = 0.4*MM_PER_INCH;

HEX_HEAD_DIAM   = 5/8*MM_PER_INCH;
HEX_HEAD_HEIGHT = 0.25*MM_PER_INCH;

MONKEY_BAR_DIAM = 0.5*MM_PER_INCH;
MONKEY_BAR_HEIGHT = 1*MM_PER_INCH;
MONKEY_BAR_TOP_HOLE_DIAM = 0.203*MM_PER_INCH; //1/4-20 tap hole

MONKEY_BAR_SET_SCREW_HOLE_DIAM = 0.1120*MM_PER_INCH; //4-40 tap hole
MONKEY_BAR_SET_SCREW_OFFSET_Z  = 0.25*MM_PER_INCH;

ALL_THRU_HOLE_DIAM = 0.1120*MM_PER_INCH; //4-40 tap hole



difference(){
    union(){
        ScrewThread(SCREW_DIAM,SCREW_HEIGHT,pitch=SCREW_PITCH);
        translate([0,0,SCREW_HEIGHT -0.01])
        cylinder(h=HEX_HEAD_HEIGHT, r=HEX_HEAD_DIAM/2, $fn=6);
        translate([0,0,SCREW_HEIGHT + HEX_HEAD_HEIGHT -0.01])
        cylinder(h=MONKEY_BAR_HEIGHT,r=MONKEY_BAR_DIAM/2);
    }
    //all thru top hole
    translate([0,0,-0.01])
    cylinder(h=3*MONKEY_BAR_HEIGHT,r=ALL_THRU_HOLE_DIAM/2);
    //just through monly bar (say for hollowing
    translate([0,0,SCREW_HEIGHT + HEX_HEAD_HEIGHT +0.01])
    cylinder(h=MONKEY_BAR_HEIGHT,r=MONKEY_BAR_TOP_HOLE_DIAM/2);
    
    //side, set screw hole
    translate([0,0,SCREW_HEIGHT + HEX_HEAD_HEIGHT + MONKEY_BAR_HEIGHT - MONKEY_BAR_SET_SCREW_OFFSET_Z])
    rotate([90,0,0])
    #cylinder(h=3*MONKEY_BAR_HEIGHT,r=MONKEY_BAR_SET_SCREW_HOLE_DIAM/2,center=true);
}
