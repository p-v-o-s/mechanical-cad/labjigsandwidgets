use <threads.scad>


$fn = 100;
MM_PER_INCH = 25.4;

JAR_THREAD_OUTER_DIAM   = 47*2;
JAR_THREAD_PITCH        = 2.5;
JAR_THREAD_TOOTH_HEIGHT = 2.0; //0 means use default
JAR_THREAD_TOOTH_ANGLE  = 30;

JAR_THREAD_INNER_DIAM   = JAR_THREAD_OUTER_DIAM - JAR_THREAD_TOOTH_HEIGHT/tan(JAR_THREAD_TOOTH_ANGLE);
JAR_THREAD_HEIGHT = 6;
JAR_THREAD_Z_OFFSET_FROM_TOP = 5;


JAR_BODY_OUTER_DIAM = JAR_THREAD_INNER_DIAM;
JAR_BODY_HEIGHT     = 50*2;
JAR_WALL_THICKNESS  = 1*2;
JAR_CAVITY_DIAM     = JAR_BODY_OUTER_DIAM - 2*JAR_WALL_THICKNESS;
JAR_THREAD_TOLERANCE = 0.0; //do not expand





JAR_THREAD_BOTTOM_Z = JAR_BODY_HEIGHT-JAR_THREAD_HEIGHT-JAR_THREAD_Z_OFFSET_FROM_TOP;


CAP_GAP = 0.25;
CAP_THREAD_Z_OFFSET_FROM_BOTTOM = 2;

CAP_THREAD_OUTER_DIAM   = JAR_THREAD_OUTER_DIAM;
CAP_THREAD_PITCH        = JAR_THREAD_PITCH;
CAP_THREAD_TOOTH_HEIGHT = 2.0; //0 means use default
CAP_THREAD_TOOTH_ANGLE  = JAR_THREAD_TOOTH_ANGLE;
CAP_THREAD_INNER_DIAM   = CAP_THREAD_OUTER_DIAM - CAP_THREAD_TOOTH_HEIGHT/tan(CAP_THREAD_TOOTH_ANGLE);
CAP_THREAD_TOLERANCE    = 0.5;
CAP_THREAD_HEIGHT = JAR_THREAD_HEIGHT;

CAP_GASKET_COMPRESSION_GAP = 1.0;
CAP_CAVITY_HEIGHT = CAP_THREAD_Z_OFFSET_FROM_BOTTOM + CAP_THREAD_HEIGHT + JAR_THREAD_Z_OFFSET_FROM_TOP + CAP_GASKET_COMPRESSION_GAP;
CAP_HEIGHT =  CAP_CAVITY_HEIGHT + 3*JAR_WALL_THICKNESS;
CAP_INNER_DIAM = JAR_BODY_OUTER_DIAM + 2*CAP_GAP;
CAP_OUTER_DIAM = CAP_THREAD_OUTER_DIAM + 2*2*JAR_WALL_THICKNESS;


CAP_CENTER_MINK_R = 3;
CAP_CENTER_DIAM1 = CAP_INNER_DIAM - 4*JAR_WALL_THICKNESS;
CAP_CENTER_DIAM2 = CAP_INNER_DIAM - 2*JAR_WALL_THICKNESS;
CAP_CENTER_HEIGHT = JAR_THREAD_Z_OFFSET_FROM_TOP + 2*JAR_WALL_THICKNESS;
CAP_CENTER_Z = CAP_CAVITY_HEIGHT - CAP_CENTER_HEIGHT + 0.01;




CAP_STOP_HEIGHT = JAR_THREAD_HEIGHT;
CAP_STOP_Z_OFFSET_FROM_THREAD_BOTTOM = 1.5;
CAP_STOP_BOTTOM_Z = JAR_THREAD_BOTTOM_Z - CAP_STOP_HEIGHT  - CAP_STOP_Z_OFFSET_FROM_THREAD_BOTTOM;

CAP_KNURL_NUM  = 10;
CAP_KNURL_DIAM = 5;


//ribs have a dependency on the cap center height
JAR_RIB_OFFSET_Z = 5;
JAR_N_RIBS = 3;
JAR_RIB_HEIGHT    = JAR_BODY_HEIGHT-JAR_WALL_THICKNESS - CAP_CENTER_HEIGHT - JAR_RIB_OFFSET_Z;
JAR_RIB_RADIUS = 8*2;
JAR_RIB_THICKNESS = 2*2;


SHAFT_COUPLER_MINK_R = 1;
SHAFT_COUPLER_WIDTH = 15.5;
SHAFT_COUPLER_THICKNESS  = SHAFT_COUPLER_WIDTH/4;
SHAFT_COUPLER_YOKE_LENGTH = SHAFT_COUPLER_WIDTH;
SHAFT_COUPLER_BASE_DIAM = SHAFT_COUPLER_WIDTH - SHAFT_COUPLER_MINK_R;
SHAFT_COUPLER_BASE_HEIGHT = SHAFT_COUPLER_WIDTH;
SHAFT_COUPLER_BORE_DIAM = 6;
SHAFT_COUPLER_SET_SCREW_TAP_DIAM   = 0.089*MM_PER_INCH;  //4-40 tap
SHAFT_COUPLER_SCREW_THRU_HOLE_DIAM = 0.1285*MM_PER_INCH; //4-40 free fit
SHAFT_COUPLER_YOKE_GAP = 0.25;

COUPLER_TAB_HEIGHT      = SHAFT_COUPLER_WIDTH;
COUPLER_TAB_WIDTH       = SHAFT_COUPLER_WIDTH + 4*SHAFT_COUPLER_MINK_R + 2*SHAFT_COUPLER_YOKE_GAP;
COUPLER_TAB_THICKNESS   = SHAFT_COUPLER_THICKNESS;

COUPLER_TAB_BASE_WIDTH  = COUPLER_TAB_WIDTH;
COUPLER_TAB_BASE_HEIGHT = SHAFT_COUPLER_WIDTH/4;


GRATING_DIAM   = JAR_CAVITY_DIAM - JAR_WALL_THICKNESS;
GRATING_HEIGHT = 13;
GRATING_RING_THICKNESS = 2;
GRATING_RING_SPACING = 2.3;

module jar_cavity(){
    translate([0,0,JAR_WALL_THICKNESS-0.01])
    cylinder(h=JAR_WALL_THICKNESS,r1=JAR_CAVITY_DIAM/2-JAR_WALL_THICKNESS,r2=JAR_CAVITY_DIAM/2);
    translate([0,0,2*JAR_WALL_THICKNESS-0.02])
    cylinder(h=JAR_BODY_HEIGHT,r=JAR_CAVITY_DIAM/2);
   
}

module rib(r = JAR_RIB_RADIUS, 
           h = JAR_RIB_HEIGHT,
           t = JAR_RIB_THICKNESS,
           trunc_ratio = 1.25,
          ){
    rotate([0,0,90])
        translate([0,r,0])

    difference(){
        translate([0,0,h/2])
        cube([2*r,2*r,h],center=true);
        //negative
        translate([-r-t/2,0,-0.01])
        cylinder(h=h+0.02,r=r);
        translate([ r+t/2,0,-0.01])
        cylinder(h=h+0.02,r=r);
        //truncate
        translate([0,trunc_ratio*r,h/2])
        cube([2.01*r,2.01*r,h+0.02],center=true);
    }
}

module rounded_cube(size,mink_r){
    minkowski(){
        cube([size[0]-2*mink_r,size[1]-2*mink_r,size[2]-2*mink_r],center=true);
        sphere(mink_r);
    }
}


module jar_thread(){
    //split screws
    ScrewThread(JAR_THREAD_OUTER_DIAM,JAR_THREAD_HEIGHT, 
                pitch=2*JAR_THREAD_PITCH,
                tooth_height=JAR_THREAD_TOOTH_HEIGHT,
                tooth_angle=JAR_THREAD_TOOTH_ANGLE,
                tolerance = JAR_THREAD_TOLERANCE
               );
    rotate([0,0,180])
    ScrewThread(JAR_THREAD_OUTER_DIAM,JAR_THREAD_HEIGHT, 
                pitch=2*JAR_THREAD_PITCH,
                tooth_height=JAR_THREAD_TOOTH_HEIGHT,
                tooth_angle=JAR_THREAD_TOOTH_ANGLE,
                tolerance = JAR_THREAD_TOLERANCE
               );
}

module jar(){
    difference(){
        //body
        union(){
            cylinder(h=JAR_WALL_THICKNESS,r1=JAR_BODY_OUTER_DIAM/2-JAR_WALL_THICKNESS,r2=JAR_BODY_OUTER_DIAM/2);
            translate([0,0,JAR_WALL_THICKNESS-0.01])
            cylinder(h=JAR_BODY_HEIGHT-JAR_WALL_THICKNESS,r=JAR_BODY_OUTER_DIAM/2);
            //split screws
            translate([0,0,JAR_THREAD_BOTTOM_Z])
            jar_thread();
            //cap stop
            translate([0,0,CAP_STOP_BOTTOM_Z])
            cylinder(h=CAP_STOP_HEIGHT,r1=JAR_BODY_OUTER_DIAM/2,r2=CAP_OUTER_DIAM/2);
        }
        //cavity
        translate([0,0,-0.01])
        jar_cavity();
    }
    //ribs
    intersection(){
        for (i = [0:JAR_N_RIBS]){
            rotate([0,0,360/JAR_N_RIBS*i])
            translate([JAR_BODY_OUTER_DIAM/2,0,JAR_RIB_OFFSET_Z])
            rib();
        }
        jar_cavity();
    }
    //coupler base
    translate([0,0,-COUPLER_TAB_BASE_HEIGHT/2 + SHAFT_COUPLER_MINK_R])
    rounded_cube([COUPLER_TAB_BASE_WIDTH,COUPLER_TAB_BASE_WIDTH,COUPLER_TAB_BASE_HEIGHT],SHAFT_COUPLER_MINK_R);
    //coupler tab
    translate([0,0,-COUPLER_TAB_HEIGHT/2 + SHAFT_COUPLER_MINK_R])
    difference(){
        rounded_cube([COUPLER_TAB_WIDTH,COUPLER_TAB_THICKNESS,COUPLER_TAB_HEIGHT],SHAFT_COUPLER_MINK_R);
        //SCREW HOLE
        translate([0,0,-SHAFT_COUPLER_MINK_R -SHAFT_COUPLER_YOKE_GAP])
        rotate([90,0,0])
        #cylinder(h=2*SHAFT_COUPLER_WIDTH,r=SHAFT_COUPLER_SCREW_THRU_HOLE_DIAM/2,center=true);
    }
    //coupler tab guides
    translate([COUPLER_TAB_BASE_WIDTH/2,0,-COUPLER_TAB_HEIGHT/2+SHAFT_COUPLER_MINK_R])
    rotate([0,0,90])
    rounded_cube([COUPLER_TAB_WIDTH,COUPLER_TAB_THICKNESS,COUPLER_TAB_HEIGHT],SHAFT_COUPLER_MINK_R);
    translate([-COUPLER_TAB_BASE_WIDTH/2,0,-COUPLER_TAB_HEIGHT/2+SHAFT_COUPLER_MINK_R])
    rotate([0,0,90])
    rounded_cube([COUPLER_TAB_WIDTH,COUPLER_TAB_THICKNESS,COUPLER_TAB_HEIGHT],SHAFT_COUPLER_MINK_R);
}



module cap_center(){
    translate([0,0,CAP_CENTER_MINK_R])
    minkowski(){
        cylinder(h=CAP_CENTER_HEIGHT-2*CAP_CENTER_MINK_R,r1=CAP_CENTER_DIAM1/2-CAP_CENTER_MINK_R,r2=CAP_CENTER_DIAM2/2-CAP_CENTER_MINK_R);
        sphere(CAP_CENTER_MINK_R);
    }
}


//module cap_body(){
//    difference(){
//        cylinder(h=CAP_HEIGHT,r=CAP_OUTER_DIAM/2);
//        //core of cap
//        translate([0,0,-0.01])
//        cylinder(h=CAP_HEIGHT-JAR_WALL_THICKNESS,r=CAP_INNER_DIAM/2);
//    }
//}

module cap_body(){
    cylinder(h=CAP_HEIGHT,r=CAP_OUTER_DIAM/2);
}

module cap_thread(){
    //exit of thread
    ClearanceHole(JAR_THREAD_OUTER_DIAM+2*CAP_GAP,
                  height=CAP_CAVITY_HEIGHT - CAP_THREAD_Z_OFFSET_FROM_BOTTOM - CAP_THREAD_HEIGHT + 0.01,
                  tolerance=0,
                  position=[0,0,CAP_THREAD_Z_OFFSET_FROM_BOTTOM+CAP_THREAD_HEIGHT]
                 )
    //split-screw hole
    ScrewHole(CAP_THREAD_OUTER_DIAM,CAP_THREAD_HEIGHT, 
                pitch=2*CAP_THREAD_PITCH,
                tooth_height=CAP_THREAD_TOOTH_HEIGHT,
                tooth_angle=CAP_THREAD_TOOTH_ANGLE,
                tolerance=CAP_THREAD_TOLERANCE,
                position=[0,0,CAP_THREAD_Z_OFFSET_FROM_BOTTOM]
               )
    rotate([0,0,180])
    ScrewHole(CAP_THREAD_OUTER_DIAM,CAP_THREAD_HEIGHT, 
                pitch=2*CAP_THREAD_PITCH,
                tooth_height=CAP_THREAD_TOOTH_HEIGHT,
                tooth_angle=CAP_THREAD_TOOTH_ANGLE,
                tolerance=CAP_THREAD_TOLERANCE,
                position=[0,0,CAP_THREAD_Z_OFFSET_FROM_BOTTOM]
               )
    //entrance to thread
    ClearanceHole(JAR_THREAD_OUTER_DIAM+2*CAP_GAP,
                  height=CAP_THREAD_Z_OFFSET_FROM_BOTTOM+0.01,
                  tolerance=0,
                  position=[0,0,-0.01]
                 )
    cap_body();
}

module cap(){
    difference(){
        union(){
            cap_thread();
            //cap center
            translate([0,0,CAP_CENTER_Z+0.01])
            cap_center();
        }
        //divet
        translate([0,0,CAP_CAVITY_HEIGHT - CAP_CENTER_HEIGHT + 2*JAR_WALL_THICKNESS])
        #cylinder(h=CAP_CENTER_HEIGHT+ 2*JAR_WALL_THICKNESS,r1=CAP_CENTER_DIAM1/2-2*JAR_WALL_THICKNESS,r2=CAP_CENTER_DIAM2/2-2*JAR_WALL_THICKNESS);
    }
   
    
    //knurls
    intersection(){
        for (i = [0:CAP_KNURL_NUM]){
            rotate([0,0,360/CAP_KNURL_NUM*i])
            translate([CAP_OUTER_DIAM/2,0,0])
            cylinder(h=CAP_HEIGHT,r=CAP_KNURL_DIAM/2);
        }
        //exclude from thread area
        difference(){
            cylinder(h=CAP_HEIGHT,r=(CAP_OUTER_DIAM+CAP_KNURL_DIAM)/2);
            translate([0,0,-0.01])
            cylinder(h=CAP_HEIGHT+0.02,r=CAP_OUTER_DIAM/2);
        }
    }
}

module shaft_coupler(){
    //shaft coupler
    difference(){
        union(){
            //bottom round base
            cylinder(h=SHAFT_COUPLER_BASE_HEIGHT,r=SHAFT_COUPLER_BASE_DIAM/2);
            //top yoke
            translate([0,0,SHAFT_COUPLER_BASE_HEIGHT + SHAFT_COUPLER_YOKE_LENGTH/2 - 0.01])
            rounded_cube([SHAFT_COUPLER_WIDTH,SHAFT_COUPLER_WIDTH,SHAFT_COUPLER_YOKE_LENGTH],SHAFT_COUPLER_MINK_R);
        
        }
        //bore hole
        translate([0,0,-SHAFT_COUPLER_THICKNESS-0.01])
        cylinder(h=SHAFT_COUPLER_WIDTH+0.02,r=SHAFT_COUPLER_BORE_DIAM/2);
        //set screw hole
        translate([0,0,SHAFT_COUPLER_WIDTH/2])
        rotate([90,0,0])
        cylinder(h=2*SHAFT_COUPLER_WIDTH,r=SHAFT_COUPLER_SET_SCREW_TAP_DIAM/2,center=true);
        //yoke slot
        translate([0,0,SHAFT_COUPLER_BASE_HEIGHT + SHAFT_COUPLER_YOKE_LENGTH/2 + SHAFT_COUPLER_THICKNESS])
        #cube([10*SHAFT_COUPLER_THICKNESS,SHAFT_COUPLER_THICKNESS+2*SHAFT_COUPLER_YOKE_GAP,SHAFT_COUPLER_YOKE_LENGTH + 0.01],center=true);
        //yoke screw holes
        translate([0,0,SHAFT_COUPLER_BASE_HEIGHT + SHAFT_COUPLER_YOKE_LENGTH - SHAFT_COUPLER_MINK_R - COUPLER_TAB_BASE_HEIGHT/2])
        rotate([90,0,0])
        #cylinder(h=2*SHAFT_COUPLER_WIDTH,r=SHAFT_COUPLER_SET_SCREW_TAP_DIAM/2,center=true);
    }
    
}


module ring(diam,thickness,height){
    difference(){
        cylinder(h=height, r=diam/2, center = true);
        cylinder(h=height+0.1, r=(diam-thickness)/2, center = true);
    }
}


module rib_tool_scaled(scale_xy=1.0){
    for (i = [0:JAR_N_RIBS]){
        rotate([0,0,360/JAR_N_RIBS*i])
        translate([JAR_BODY_OUTER_DIAM/2,0,0])
        scale([scale_xy,scale_xy,1.0])
        rib();
    
    }
}

module rib_tool(r,h,t,trunc_ratio){
    for (i = [0:JAR_N_RIBS]){
        rotate([0,0,360/JAR_N_RIBS*i])
        translate([JAR_BODY_OUTER_DIAM/2,0,0])
        rib(r,h,t,trunc_ratio);  
    }
}

module arc(r1, r2, a1, a2) {
  difference() {
    difference() {
      polygon([[0,0], [cos(a1) * (r1 + 50), sin(a1) * (r1 + 50)], [cos(a2) * (r1 + 50), sin(a2) * (r1 + 50)]]);
      circle(r = r2);
    }
    difference() {
      circle(r=r1 + 100);
      circle(r=r1);
    }
  }
}

module cylinder_arc(r1,r2,a1,a2,h){
    linear_extrude(height = h) {
      arc(r1, r2, a1, a2);
    }
}

module rib_slots(){
    r1 = JAR_RIB_RADIUS + 2*JAR_RIB_THICKNESS;
    r2 = JAR_RIB_THICKNESS;
    a  = 27.5;
    x = JAR_CAVITY_DIAM/2 - JAR_RIB_RADIUS - 1.75*JAR_RIB_THICKNESS;
    for (i = [0:JAR_N_RIBS]){
        rotate([0,0,360/JAR_N_RIBS*i])
        translate([x,0,-GRATING_HEIGHT/2])
        cylinder_arc(1.1*r1,1.1*r2,-a,a,2*GRATING_HEIGHT); 
    }

}


module grating_insert(diam = GRATING_DIAM,
                      ring_thickness = GRATING_RING_THICKNESS,
                      ring_spacing = GRATING_RING_SPACING,
                      height = GRATING_HEIGHT
                      ){
    

    difference(){
        translate([0,0,height - ring_thickness/2])
        union(){
            //ring around edge
            intersection(){
                translate([0,0,-height/2 + ring_thickness/2])
                cylinder(h=height, r=diam/2, center = true);
                translate([0,0,-JAR_RIB_HEIGHT/2])
                rib_tool_scaled(2.2);
            }
            
            //grating
            n_rings = diam/(2*ring_spacing);
            v0 = [diam/2,diam/2,0];
            for (i = [0:n_rings]){
                ring(diam=2*i*ring_spacing, thickness=ring_spacing,height=ring_thickness + 0.1);
            }
            ////support spokes
            //for (i = [0:3]){
            //    rotate([90,0,i*120 + 30])
            //    cylinder(h=diam/2,r1=ring_thickness/4,r2=ring_thickness/2);
            //}
        }
        //subtractions
        //rib slots
        rib_tool(r=JAR_RIB_RADIUS,h=JAR_RIB_HEIGHT,t=1.5*JAR_RIB_THICKNESS,trunc_ratio=1.30);
        //#rib_slots();
        //carve out
        translate([0,0,-ring_thickness + height/2])
        ring(1.1*diam,3/4*diam,height);
        
    }
    
}


module assembly(sep=0){
    translate([0,0,-COUPLER_TAB_BASE_HEIGHT/2 -SHAFT_COUPLER_MINK_R -SHAFT_COUPLER_LENGTH-SHAFT_COUPLER_YOKE_GAP-sep])
    shaft_coupler();
    jar();
    translate([0,0,CAP_STOP_BOTTOM_Z+CAP_STOP_HEIGHT+sep])
    cap();
    
}

//components (for testing)
//rib();
//jar_thread();
//translate([0,0,20])
//cap_thread();

//negative of cap thread
//translate([0,0,10])
//difference(){
//    translate([0,0,0.01])
//    cylinder(h=CAP_HEIGHT-0.02,r=CAP_THREAD_OUTER_DIAM/2);
//    cap_thread();  
//}
//cap_center();


//jar();
//cap();
shaft_coupler();
//fits 40ml polypropylene specimen container, comes up to 20ml mark
//grating_insert();

//assembly(sep=20);


//rib_slots();
