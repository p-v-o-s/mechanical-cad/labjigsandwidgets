use <threads.scad>


$fn = 100;
MM_PER_INCH = 25.4;





MONKEY_BAR_DIAM = 0.5*MM_PER_INCH;
MONKEY_BAR_HEIGHT = 50;
MONKEY_BAR_TAP_HOLE_DIAM  = 0.203*MM_PER_INCH; //for 1/4-20 tap
MONKEY_BAR_VENT_HOLE_DIAM = 0.1*MM_PER_INCH;

MOUNT_PLATE_X_LENGTH = 46 + MONKEY_BAR_DIAM;
MOUNT_PLATE_Y_LENGTH = 32;
MOUNT_PLATE_THICKNESS = 3;
MOUNT_PLATE_SCREW_OFFSET_X = 8;
MOUNT_PLATE_SCREW_OFFSET_Y = 7;
MOUNT_PLATE_SCREW_SEP_X = 33;
MOUNT_PLATE_SCREW_SEP_Y = 18;

MOUNT_PLATE_SCREW_THRU_DIAM = 3.5;

MOTOR_SHAFT_COLLAR_DIAM = 6;
MOTOR_SHAFT_COLLAR_THRU_DIAM = MOTOR_SHAFT_COLLAR_DIAM + 0.5;
MOTOR_SHAFT_COLLAR_OFFSET_X = 32;







module mount_plate(){
    difference(){
        cube([MOUNT_PLATE_X_LENGTH,MOUNT_PLATE_Y_LENGTH,MOUNT_PLATE_THICKNESS]);
        //screw pass through holes
        X1 = MOUNT_PLATE_SCREW_OFFSET_X;
        Y1 = MOUNT_PLATE_SCREW_OFFSET_Y;
        translate([X1,Y1,-MOUNT_PLATE_THICKNESS])
            #cylinder(h=3*MOUNT_PLATE_THICKNESS,r=MOUNT_PLATE_SCREW_THRU_DIAM/2);
        X2 = X1;
        Y2 = Y1 + MOUNT_PLATE_SCREW_SEP_Y;
        translate([X2,Y2,-MOUNT_PLATE_THICKNESS])
            #cylinder(h=3*MOUNT_PLATE_THICKNESS,r=MOUNT_PLATE_SCREW_THRU_DIAM/2);
        X3 = X1 + MOUNT_PLATE_SCREW_SEP_X;
        Y3 = Y1;
        translate([X3,Y3,-MOUNT_PLATE_THICKNESS])
            #cylinder(h=3*MOUNT_PLATE_THICKNESS,r=MOUNT_PLATE_SCREW_THRU_DIAM/2);
        X4 = X3;
        Y4 = Y2;
        translate([X4,Y4,-MOUNT_PLATE_THICKNESS])
            #cylinder(h=3*MOUNT_PLATE_THICKNESS,r=MOUNT_PLATE_SCREW_THRU_DIAM/2);
        //motor shaft through hole
        X5 = MOTOR_SHAFT_COLLAR_OFFSET_X;
        Y5 = MOUNT_PLATE_Y_LENGTH/2;
        translate([X5,Y5,-MOUNT_PLATE_THICKNESS])
            #cylinder(h=3*MOUNT_PLATE_THICKNESS,r=MOTOR_SHAFT_COLLAR_THRU_DIAM/2);
    }
}

module monkey_bar(){
    difference(){
        union(){
            hull(){
                rotate([0,90,0])
                cylinder(h=MONKEY_BAR_HEIGHT/2,r=MONKEY_BAR_DIAM/2);
                cube([MONKEY_BAR_DIAM,MONKEY_BAR_DIAM,MOUNT_PLATE_THICKNESS],center=true);
            }
            //extension
            translate([MONKEY_BAR_HEIGHT/2-0.001,0,0])
            rotate([0,90,0])
            cylinder(h=MONKEY_BAR_HEIGHT/2,r=MONKEY_BAR_DIAM/2);
        }
        //tap hole
        translate([MONKEY_BAR_DIAM,0,0])
        rotate([0,90,0])
        cylinder(h=2*MONKEY_BAR_HEIGHT,r=MONKEY_BAR_TAP_HOLE_DIAM/2);
        //vent holes
        translate([MONKEY_BAR_DIAM+MONKEY_BAR_VENT_HOLE_DIAM/2,0,0])
        rotate([90,0,0])
        cylinder(h=2*MONKEY_BAR_HEIGHT,r=MONKEY_BAR_VENT_HOLE_DIAM/2,center=true);
        
    }
}


module part(){
    translate([-MOUNT_PLATE_X_LENGTH,-MOUNT_PLATE_Y_LENGTH/2,-MOUNT_PLATE_THICKNESS/2])
    mount_plate();
    monkey_bar();
}

//mount_plate();
//monkey_bar();
part();