$fn=100;


SCREW_OFFSET_X = 10;
SCREW_HOLE_SEP = 42.5;
SCREW_HOLE_DIAM = 4;
SCREW_HEAD_DIAM = 5.5;
SCREW_SINK_DEPTH = 3;


LENGTH = SCREW_HOLE_SEP + 2*SCREW_OFFSET_X;
WIDTH = 5;
HEIGHT = 30;

FLANGE_DEPTH = 1.5;
FLANGE_OFFSET_FROM_CENTER_Y = 5.5;

module screw_hole(){
    rotate([90,0,0])
    union(){
        cylinder(h=3*WIDTH,r=SCREW_HOLE_DIAM/2,center=true);
        //translate([SCREW_OFFSET_X,0,HEIGHT/2])
        cylinder(h=2*SCREW_SINK_DEPTH,r=1.1*SCREW_HEAD_DIAM/2,center=true);
    }
}

difference(){
    cube([LENGTH,WIDTH,HEIGHT]);
    //screw holes
    translate([SCREW_OFFSET_X,0,HEIGHT/2])
    screw_hole();
    translate([SCREW_OFFSET_X+SCREW_HOLE_SEP,0,HEIGHT/2])
    screw_hole();
    //flange
    translate([-0.01,WIDTH - FLANGE_DEPTH + 0.01,-0.01])
    cube([LENGTH+0.02,FLANGE_DEPTH,HEIGHT/2-FLANGE_OFFSET_FROM_CENTER_Y]);
}