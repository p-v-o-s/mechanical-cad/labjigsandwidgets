$fn=100;

MM_PER_INCH = 25.4;

SCREW_TAP_HOLE_DIAM_4_40  = 0.0890*MM_PER_INCH;
SCREW_THRU_HOLE_DIAM_4_40 = 0.1285*MM_PER_INCH;

M = 8;
N = 8;
PLUG_DIAM = 3;
SOCKET_H = 2;
SOCKET_SPACING = 7;
SOCKET_RESTRICTION_DIAM = 0.1;
SOKCET_HOLE_DIAM = PLUG_DIAM - SOCKET_RESTRICTION_DIAM;
SOCKET_THICKNESS_R = 1;
SOCKET_BODY_DIAM1 = SOKCET_HOLE_DIAM + 2*SOCKET_THICKNESS_R;
SOCKET_BODY_DIAM2 = 0.90*SOCKET_BODY_DIAM1;
MEMBRANE_BODY_THICKNESS = 1;
MEMBRANE_BODY_LENGTH    = (M+1)*SOCKET_SPACING;
MEMBRANE_BODY_WIDTH     = (N+1)*SOCKET_SPACING;

SUPPORT_WALL_THICKNESS = 2;
SUPPORT_LENGTH = MEMBRANE_BODY_LENGTH + 2*SUPPORT_WALL_THICKNESS;
SUPPORT_WIDTH  = MEMBRANE_BODY_WIDTH + 2*SUPPORT_WALL_THICKNESS;
SUPPORT_HEIGHT = MEMBRANE_BODY_THICKNESS + SOCKET_H - 0.02;


SUPPORT_STANDOFF_DIAM = 7;
SUPPORT_STANDOFF_H    = SUPPORT_HEIGHT + 5;


module plug_array_membrane(with_socket_holes=true,with_conic_sockets=true){
    translate([-MEMBRANE_BODY_LENGTH/2,-MEMBRANE_BODY_WIDTH/2,0])
    difference(){
        union(){
            cube([MEMBRANE_BODY_LENGTH,MEMBRANE_BODY_WIDTH,MEMBRANE_BODY_THICKNESS]);
            for(m = [1:M]){
                for(n = [1:N]){
                    x = m*SOCKET_SPACING;
                    y = n*SOCKET_SPACING;
                    translate([x,y,MEMBRANE_BODY_THICKNESS - 0.01])
                    if(with_conic_sockets){
                        cylinder(h=SOCKET_H + 0.01,r1=SOCKET_BODY_DIAM1/2,r2=SOCKET_BODY_DIAM2/2);
                    }else{
                        cylinder(h=SOCKET_H + 0.01,r=SOCKET_BODY_DIAM1/2);
                    }
                }
            }
        }
        //cavities
        for(m = [1:M]){
            for(n = [1:N]){
                x = m*SOCKET_SPACING;
                y = n*SOCKET_SPACING;
                if (with_socket_holes){
                    translate([x,y,-0.01])
                    cylinder(h=MEMBRANE_BODY_THICKNESS + SOCKET_H + 0.02,r=SOKCET_HOLE_DIAM/2);
                }
            }
        }
       
    }
    
}

module plug_array_membrane_with_cutouts(with_socket_holes=true,with_conic_sockets=true){
    difference(){
        plug_array_membrane(with_socket_holes,with_conic_sockets);
        //cutouts
        //standoffs
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
    }
    
}

MEMBRANE_MOLD_THICKNESS = 5;
MEMBRANE_MOLD_LENGTH    = SUPPORT_LENGTH + 3;
MEMBRANE_MOLD_WIDTH     = SUPPORT_WIDTH  + 3;

module plug_array_membrane_mold(){
    difference(){
        union(){
            translate([-MEMBRANE_MOLD_LENGTH/2,-MEMBRANE_MOLD_WIDTH/2,+0.01])
            cube([MEMBRANE_MOLD_LENGTH,MEMBRANE_MOLD_WIDTH,MEMBRANE_MOLD_THICKNESS]);
            
        }
        //cavities
        plug_array_membrane_with_cutouts();
        //tap holes for standoffs
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
       
    }  
    
}


module plug_array_support_bottom(){
    difference(){
        union(){
            translate([-SUPPORT_LENGTH/2,-SUPPORT_WIDTH/2,+0.01])
            cube([SUPPORT_LENGTH,SUPPORT_WIDTH,SUPPORT_HEIGHT]);
            //standoffs
            translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, 0])
            cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
            translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, 0])
            cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
            translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, 0])
            cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);
            translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, 0])
            cylinder(r=SUPPORT_STANDOFF_DIAM/2,h=SUPPORT_STANDOFF_H);

            
        }
        //cavities
        plug_array_membrane_with_cutouts(with_socket_holes=false,with_conic_sockets=false);
        //tap holes for standoffs
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
       
    }  
    
}

module plug_array_support_top(){
    difference(){
        union(){
            translate([-SUPPORT_LENGTH/2,-SUPPORT_WIDTH/2,+0.01])
            cube([SUPPORT_LENGTH,SUPPORT_WIDTH,SUPPORT_HEIGHT]);
            
        }
        //cavities
        plug_array_membrane_with_cutouts(with_socket_holes=false,with_conic_sockets=false);
        //tap holes for standoffs
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,SUPPORT_WIDTH/2 - SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([SUPPORT_LENGTH/2 - SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
        translate([-SUPPORT_LENGTH/2 + SUPPORT_STANDOFF_DIAM/2,-SUPPORT_WIDTH/2 + SUPPORT_STANDOFF_DIAM/2, -0.01])
        cylinder(r=SCREW_TAP_HOLE_DIAM_4_40/2,h=SUPPORT_STANDOFF_H+0.02);
       
    }  
    
}

module assembly(){   
    translate([0,0,-10])
        plug_array_membrane_with_cutouts();
    plug_array_support_bottom();
    translate([0,0,-20])
        plug_array_support_top();
}

//assembly();
//plug_array_support_bottom();
//plug_array_support_top();
plug_array_membrane_mold();

