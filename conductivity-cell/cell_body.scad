$fn=100;
INCHES_TO_MM = 25.4;

CELL_WALL_T = 5;
CELL_ID = 13;
CELL_OD = CELL_ID + 2*CELL_WALL_T;
CELL_H = 27;

SCREW_TAP_HOLE_DIAM = 0.0860*INCHES_TO_MM; //tap hole for 2-56
SCREW_HOLE_OFFSET_Z = 3.5;
SCREW_HOLE_SEP_Z    = 5;

module cell_body(od=CELL_OD,id=CELL_ID,h=CELL_H){
    difference(){
        cylinder(r=od/2,h=h);
        translate([0,0,-0.1])
        cylinder(r=id/2,h=h+0.2);
    }
}

module part(){
    difference(){
        cell_body();
        //holes
        for(i=[0:4]){
            translate([0,0,SCREW_HOLE_OFFSET_Z + SCREW_HOLE_SEP_Z*i])
            rotate([0,90,0])
            cylinder(r=SCREW_TAP_HOLE_DIAM/2,h=CELL_H,center=true);
        }
    }
}

part();