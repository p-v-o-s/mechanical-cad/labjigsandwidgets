$fn=100;
INCHES_TO_MM = 25.4;

RING_OD = 0.50*INCHES_TO_MM;
RING_ID = 3.25;
RING_H = 5;

module spacer_ring(od=RING_OD,id=RING_ID,h=RING_H){
    difference(){
        cylinder(r=od/2,h=h);
        translate([0,0,-0.1])
        cylinder(r=id/2,h=h+0.2);
    }
}

spacer_ring();